alias dir='ls --color'
alias rw='nitrogen --restore'
alias ncp='ncmpcpp'
alias ls='exa'
alias nf='neofetch'
alias pf='pfetch'

#alias nix-env='echo STOP'

alias v='vi'
alias vi='vim'
alias vim='nvim'
alias mkdir='\mkdir -pv'
alias tmux='$HOME/.config/scripts/tmux.sh'

alias arc='cd $HOME/.config/awesome/; $EDITOR rc.lua'

alias ssh46="ssh izzy@192.168.0.46"

alias please="sudo !!"
alias pls='sudo apt'

alias brc='nvim ~/.bashrc'
alias bal='nvim ~/.bash_aliases'
alias sbrc="source ~/.bashrc"

alias nv='cd $HOME/.config/nvim/; nvim init.vim filetypes.vim plugins.lua'
#alias np='nvim $HOME/.config/nvim/plugins.lua'

alias ai="sudo apt install"
alias ar="sudo apt remove"
alias ap="sudo apt purge"
alias au="sudo apt update"
alias aug="sudo apt upgrade"
alias afg="sudo apt-get dist-upgrade"
alias ary="sudo apt autoremove -y"
alias ali='apt list --installed | grep'
#alias yay='yay --color-always'

alias rm='rm -rf'

# nix
nxi () {
    nix-env -iA nixos.$1
}
alias nxu='nix-env --uninstall'

alias nsp='nix-shell -p'

# git
alias ga='git add .' \
    gm='git commit -m' \
    gp='git push' \
    gst='git status' \
    gsw='git switch'


gac() {
    git add . && git commit -m $1 && git push
}

# Shortcut for using the Kdiff3 tool for svn diffs.
alias svnkdiff3='svn diff --diff-cmd kdiff3'

# du command aliases
alias duh='du -chs .[^.]* | sort -h'
alias dua='du -chs * | sort -h'

# nixos
alias run='nix-autobahn'

alias cfg='sudo -e /etc/nixos/configuration.nix; sudo nixos-rebuild switch'

